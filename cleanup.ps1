#!/usr/bin/env pwsh
[CmdletBinding()]
param (
	# Specifies a path to one or more locations.
	[Parameter(Mandatory, HelpMessage="Path to one or more locations.")]
	[Alias("p")]
	[ValidateNotNullOrEmpty()]
	[string[]]
	$path,

	# Extension of preserve files
	[Parameter(Mandatory, HelpMessage="Extension of files to be processed.")]
	[ValidateNotNullOrEmpty()]
	[string[]]
	$ext,

	# Quantity of preserve files
	[Parameter(Mandatory, HelpMessage="Quantity of preserve files.")]
	[ValidateNotNullOrEmpty()]
	[int]
	$preserve,

	# recurse mode
	[Parameter()]
	[Alias('r')]
	[switch]
	$recurse,

	# children only mode
	[Parameter()]
	[Alias('c')]
	[switch]
	$children,

	[Parameter(HelpMessage="Log file.")]
	[Alias("l")]
	[ValidateNotNullOrEmpty()]
	[string]
	$LogFile,

	[Parameter(HelpMessage="SMTP server.")]
	[ValidateNotNullOrEmpty()]
	[string]
	$smtp,

	[Parameter(HelpMessage="To email.")]
	[ValidateNotNullOrEmpty()]
	[string]
	$to_mail,

	[Parameter(HelpMessage="From email.")]
	[ValidateNotNullOrEmpty()]
	[string]
	$from_mail,

	[Parameter(HelpMessage="Telegram chatid.")]
	[ValidateNotNullOrEmpty()]
	[string]
	$telegram_chatid,

	[Parameter(HelpMessage="Telegram token.")]
	[ValidateNotNullOrEmpty()]
	[string]
	$telegram_token,

	# Dry run mode
	[Parameter()]
	[Alias("n")]
	[switch]
	$dry = $false
)

Set-StrictMode -Version 'Latest'

# for powershell 2
if (! (Get-Variable -Name PSCommandPath -ErrorAction SilentlyContinue)) {
	$PSCommandPath = $MyInvocation.MyCommand.Definition
}

$title = 'Cleanup files'

$Script:ErrorActionPreference = [System.Management.Automation.ActionPreference]::Stop

if ($PSVersionTable.PSVersion.Major -gt 2) {
	[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
}

# logging variable
New-Variable -Name log -Force -ErrorAction SilentlyContinue

# error counter
$counter_error = 0

# dot sourcing
try
{
	. $(Join-Path -Path $(Join-Path -Path $(Split-Path -Parent -Path $PSCommandPath) -ChildPath 'pwsh-common') -ChildPath 'function.ps1')
}
catch
{
	$Command = if ($Host.Name -eq 'ConsoleHost') {
		[Console]::Error.WriteLine
	} else {
		$Host.UI.WriteErrorLine
	}
	if ($PSCommandPath) {
		[void] $Command.Invoke((Split-Path -Leaf -Path $PSCommandPath) + ' : ' + 'Dot sourcing fail')
	} else {
		[void] $Command.Invoke('Dot sourcing fail')
	}
	$counter_error++
	exit 1
}

function Send-MailNotification {
	[CmdletBinding()]
	param (
		[Parameter(Mandatory,ValueFromPipeline=$true,Position = 0)]
		[ref]
		$var
	)

	New-Variable -Name subj -Force -ErrorAction SilentlyContinue
	Join-Rows ([ref]$subj) $title
	if ($PSCommandPath) {
		Join-Rows ([ref]$subj) '(' -space
		Join-Rows ([ref]$subj) (Split-Path -Leaf -Path $PSCommandPath)
		Join-Rows ([ref]$subj) ')'
	}
	Join-Rows ([ref]$subj) ':'
	Join-Rows ([ref]$subj) ([Environment]::MachineName) -space
	if ($counter_error -gt 0) {
		Join-Rows ([ref]$subj) ("E:" + $counter_error) -space
	}

	New-Variable -Name r -Force -ErrorAction SilentlyContinue
	$r = Send-Mail -smtp $smtp -from $from_mail -to $to_mail -subj $subj -body $var

	if ($r.GetType().Name -eq 'ErrorRecord')
	{
		'mail notification fail.', $(Show-ErrorObject $r -r) | Join-Words | Logging -var ([ref]$log) -file $LogFile -stderr
	}
	elseif ($r -ne $true)
	{
		'mail notification fail.' | Join-Words | Logging -var ([ref]$log) -file $LogFile -stderr
	}
}

function Send-TelegramNotification {
	[CmdletBinding()]
	param (
		[Parameter(Mandatory=$true,ValueFromPipeline=$true,Position = 0)]
		[ref]
		$var
	)

	ForEach ($token in $telegram_token)
	{
		ForEach ($chatid in $telegram_chatid)
		{
			New-Variable -Name r -Force -ErrorAction SilentlyContinue
			$r = Send-TelegramTextMessage -BotToken $token -ChatID $chatid -Message $var
			$r
			if ($r.GetType().Name -eq 'ErrorRecord')
			{
				'telegram notification fail.', $(Show-ErrorObject $r -r) | Join-Words | Logging -var ([ref]$log) -file $LogFile -stderr
				$Script:counter_error++
			}
			elseif ( (Get-Member -InputObject $r -Name 'ok' -MemberType 'NoteProperty') -and ($r.ok -ne $true) )
			{
				'telegram notification fail.' | Join-Words | Logging -var ([ref]$log) -file $LogFile -stderr
				$Script:counter_error++
			}

		}
	}
}

New-Variable -Name Text -Force -ErrorAction SilentlyContinue
Join-Rows ([ref]$Text) ([DateTime]::Now.ToString('u').Replace('Z',''))
Join-Rows ([ref]$Text) 'Start' -space
if ($PSCommandPath) {
	Join-Rows ([ref]$Text) $(Split-Path -Leaf -Path $PSCommandPath) -space
}
Join-Rows ([ref]$Text) '::' -space
Join-Rows ([ref]$Text) ([Environment]::MachineName) -space
Join-Rows ([ref]$Text) '@' -space
Join-Rows ([ref]$Text) ([Environment]::UserName) -space
if ($dry -eq $true)
{
	Join-Rows ([ref]$Text) '::' -space
	Join-Rows ([ref]$Text) 'Dry-run mode' -space
}
$Text | Logging -var ([ref]$log) -file $LogFile
if ($LogFile.Length -gt 0)
{
	"log: $LogFile" | Logging -var ([ref]$log) -file $LogFile
}

function CleanupFiles {
	<#
		.SYNOPSIS
			Cleanup files
	#>
	param (
		# preserve quantity
		[int]
		$preserve,

		# extension
		[string]
		$ext
	)

	begin
	{
		$N = 0
		$Name = $null
		$Script:CleanedupFiles = 0
	}

	process
	{
		if ($_.Name -match ('(.*)\.' + $ext))
		{
			$CurrentName = $Matches[1]

			if ($CurrentName -ne $Name)
			{
				$N += 1
				$Name = $CurrentName
			}

			$filepath = $_.FullName

			# $fileName = "path_to_file"; $file = [System.io.File]::Open($fileName, 'Open', 'Read', 'None'); Write-Host "Press any key to continue ..."; $null = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown"); $file.Close()

			if ($N -gt $preserve) 
			{
				try
				{
					if ($dry -eq $false)
					{
						Remove-Item -Force -LiteralPath $filepath -ErrorAction Stop
						("Delete ({0} of {1}) file {3}" -f $N, $preserve, $Name, $filepath) | Logging -var ([ref]$log) -file $LogFile
					}
					else
					{
						("[Dry-run] Delete ({0} of {1}) file {3}" -f $N, $preserve, $Name, $filepath) | Logging -var ([ref]$log) -file $LogFile
					}

					$Script:CleanedupFiles += 1
				}
				catch
				{
					("Error ({0} of {1}) file {3}" -f $N, $preserve, $Name, $filepath) | Logging -var ([ref]$log) -file $LogFile -stderr
					Write-ErrorObject $_ | Logging -var ([ref]$log) -file $LogFile -stderr
					$Script:counter_error++
				}
			}
			else
			{
				("Preserve ({0} of {1}) file {3}" -f $N, $preserve, $Name, $filepath) | Logging -var ([ref]$log) -file $LogFile
			}
		}
	}

	end
	{
		#return $_
	}

}

function SortingFiles {
	<#
		.SYNOPSIS
			Listing and sorting files in a directory
	#>
	param (
		[string]
		$path,

		[int]
		$preserve,

		[string[]]
		$ext
	)

	ForEach ($item in $ext)
	{
		"dir: $path, ext: $item" | Logging -var ([ref]$log) -file $LogFile

		Get-ChildItem -LiteralPath $path | Where-Object {! $_.PSIsContainer} | Sort-Object -Property LastWriteTime -Descending | CleanupFiles -preserve $preserve -ext $item

		New-Variable -Name Text -Force -ErrorAction SilentlyContinue
		if ($dry -eq $true)
		{
			Join-Rows ([ref]$Text) "[Dry-run] "
		}
		Join-Rows ([ref]$Text) "Cleaned up files: $Script:CleanedupFiles"
		$Text | Logging -var ([ref]$log) -file $LogFile
	}

}

# Transfer to a processing directory
ForEach ($item in $path)
{
	if ($children)
	{
		# children
		Get-ChildItem -LiteralPath $item | Where-Object {$_.PSIsContainer} | ForEach-Object {
			SortingFiles -path $_.FullName -preserve $preserve -ext $ext
		}

		# children + recurse
		if ($recurse)
		{
			Get-ChildItem -LiteralPath $item | Where-Object {$_.PSIsContainer} | ForEach-Object {
				Get-ChildItem -Recurse -LiteralPath $_ | Where-Object {$_.PSIsContainer}  | ForEach-Object {
					SortingFiles -path $_.FullName -preserve $preserve -ext $ext
				}
			}
		}
	}
	else
	{
		# root
		SortingFiles -path $item -preserve $preserve -ext $ext

		# root + recurse
		if ($recurse)
		{
			Get-ChildItem -Recurse -LiteralPath $item | Where-Object {$_.PSIsContainer} | ForEach-Object {
				SortingFiles -path $_.FullName -preserve $preserve -ext $ext
			}
		}
	}
}

function Split-TelegramNotification
{
	[CmdletBinding()]
	param (
		[Parameter(Mandatory,ValueFromPipeline=$true,Position = 0)]
		[ref]
		$var,

		[Parameter()]
		[Alias("limit")]
		[int]
		$LimitLength = 4096
	)

	for ($i = 0; $i -lt [System.Math]::Floor($var.Value.Length / $LimitLength); $i++)
	{
		$msg = $var.Value.Substring($i * $LimitLength, $LimitLength)
		Send-TelegramNotification -var ([ref]$msg)
	}

	if ( $($var.Value.Length % $LimitLength) -gt 0 )
	{
		$msg = $var.Value.Substring($([System.Math]::Floor($var.Value.Length / $LimitLength)) * $LimitLength, $($var.Value.Length % $LimitLength))
		Send-TelegramNotification -var ([ref]$msg)
	}
}

# telegram
if ($counter_error -ne 0)
{
	if ($PSVersionTable.PSVersion.Major -gt 2)
	{
		if ( ($PSBoundParameters.ContainsKey('telegram_chatid')) -and ($PSBoundParameters.ContainsKey('telegram_token')) )
		{
			Split-TelegramNotification -var ([ref]$log)
		}
	}
}

# mail
if ( ($PSBoundParameters.ContainsKey('smtp')) -and ($PSBoundParameters.ContainsKey('to_mail')) -and ($PSBoundParameters.ContainsKey('from_mail')) )
{
	Send-MailNotification -var ([ref]$log)
}

# quit
if ($counter_error -ne 0)
{
	exit 1
}
else
{
	exit 0
}
